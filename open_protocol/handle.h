//
//  handle.h
//  open_protocol
//
//  Created by Eywen on 2021/2/6.
//

#ifndef handle_h
#define handle_h

#include <stdint.h>
#include <stdbool.h>
#include "open_protocol.h"

typedef  bool (*handle_ptr)(frame_t *frame);

typedef struct _handle_t{
    uint16_t fun_code;
    handle_ptr handle_fun;
}handle_t;


#define SOFT_VERTION_LEN     16

#define FILE_TRANS_PER_LEN          512



#endif /* handle_h */

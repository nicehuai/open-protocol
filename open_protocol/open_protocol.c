//
//  open_protocol.c
//  open_protocol
//
//  Created by Eywen on 2021/2/3.
//

#include "open_protocol.h"
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "dev.h"

static frame_list_t g_frame_list;       /* 保存待处理的数据帧 */

static uint16_t cal_check_code(uint8_t *data, uint16_t len);
static frame_status_t check_frame_integrity(uint8_t *frame_data, uint16_t frame_len);
static uint8_t  get_uint8(uint8_t *ptr);
static uint16_t get_uint16(uint8_t *ptr);
static uint32_t get_uint32(uint8_t *ptr);
static uint64_t get_uint64(uint8_t *ptr);
static float get_float(uint8_t *ptr);
static void  set_uint8(uint8_t *ptr, uint8_t var);
static void set_uint16(uint8_t *ptr, uint16_t var);
static void set_uint32(uint8_t *ptr, uint32_t var);
static void set_uint64(uint8_t *ptr, uint64_t var);
static void set_float(uint8_t *ptr, float var);

static void frame_list_read_index_up(frame_list_t *frame_list);
static void frame_list_write_index_up(frame_list_t *frame_list);
static void frame_list_count_up(frame_list_t *frame_list);
static void frame_list_count_down(frame_list_t *frame_list);
static void frame_list_pop(frame_list_t *frame_list, frame_t *frame);
static bool frame_list_is_full(frame_list_t *frame_list);
static bool frame_list_is_empty(frame_list_t *frame_list);
static bool frmae_list_append(frame_list_t *frame_list, uint8_t *frame_std_buf, uint16_t buf_len);


bool handle_frame_buff(frame_list_t *frame_list)
{
    frame_t frame;
    /* 帧缓存中不为空 */
    if(frame_list_is_empty(frame_list) == false){
        frame_list_pop(frame_list, &frame);
        
        /* 处理帧 */
        
    }
    return true;
}

/* 将接收到的数据提取出来并存储到内存 */
frame_status_t deframe(uint8_t *frame_data, uint16_t frame_len)
{
    uint16_t i;
    uint16_t data_len, one_frame_len;
    uint16_t head_pos, tail_pos;
   
    if(frame_len < FRAME_MIN_LEN){
        return error_integrity;
    }
    
    /* 查找帧头和帧尾 */
    for(i = 0; i <= (frame_len - FRAME_MIN_LEN); i++){
        /* 匹配帧头的位置 */
        head_pos = i;
        if(get_uint16(frame_data + head_pos) != FRAME_HEAD){
            continue;
        }
        
        data_len = get_uint16(frame_data + i + FRAME_DATA_LEN_OFFSET);
        one_frame_len = data_len + FRAME_MIN_LEN;
        /* 帧长度不对,继续查找帧头 */
        if(one_frame_len > frame_len){
            continue;
        }

        /* 匹配帧尾的位置 */
        tail_pos = i + data_len + FRAME_TAIL_MIN_OFFSET;
        if(get_uint16(frame_data + tail_pos) != FRAME_TAIL){
            continue;
        }
        
        /* 帧校验检测 */
        if(check_frame_integrity(frame_data + i, one_frame_len) != ok_integrity){
            continue;
        }
#if FRAME_SRC_DST
        /* 目的地址不是发给自己或非广播地址, 丢弃该数据不处理 */
        if((get_dev_addr() != get_frame_dst((frame_t *)(frame_data + i))) &&
                (get_frame_dst((frame_t *)(frame_data + i)) != FRAME_BROADCAST_ADDR)){
            i += (one_frame_len - 1);
            continue;
        }
#endif

        frmae_list_append(&g_frame_list, frame_data + i, one_frame_len);
        i += (one_frame_len - 1);
    }

    return error_integrity;
}

frame_status_t check_frame_integrity(uint8_t *frame_data, uint16_t frame_len)
{
    uint16_t data_len;
    uint16_t check_code, check_val;
    
    /* 判断帧头是否正确 */
    if(get_uint16(frame_data) != FRAME_HEAD){
        debug("frame error head");
        return error_head;
    }
    
    data_len = get_uint16(frame_data + FRAME_DATA_LEN_OFFSET);
    /* 长度错误 */
    if((data_len + FRAME_MIN_LEN) > frame_len){
        debug("frame error len");
        return  error_len;
    }
    
    /* 判断帧尾是否正确 */
    if(get_uint16(frame_data + FRAME_TAIL_MIN_OFFSET + data_len) != FRAME_TAIL){
        debug("frame error tail");
        return error_tail;
    }

    /* 计算校验数据是否损坏 */
    check_val = cal_check_code(frame_data + FRAME_CHECK_FIELD_BEGIN, FRAME_CHECK_FIELD_MIN_LEN + data_len);
    check_code = get_uint16(frame_data + FRAME_CHECK_CODE_MIN_OFFSET + data_len);
    if(check_code != check_val){
        debug("frame check error: %u != %u", check_val, check_code);
        return error_check;
    }
    
    return ok_integrity;
}

void frame_list_init(frame_list_t *frame_list)
{
    frame_list->read_index = 0;
    frame_list->write_index = 0;
    frame_list->count = 0;
}

bool frame_list_is_full(frame_list_t *frame_list)
{
    return frame_list->count == FRAME_BUF_LIST_MAX_LEN ? true : false;
}

bool frame_list_is_empty(frame_list_t *frame_list)
{
    return frame_list->count == 0 ? true : false;
}

void frame_list_read_index_up(frame_list_t *frame_list)
{
    frame_list->read_index++;
    frame_list->read_index %= FRAME_BUF_LIST_MAX_LEN;
}

void frame_list_write_index_up(frame_list_t *frame_list)
{
    frame_list->write_index++;
    frame_list->write_index %= FRAME_BUF_LIST_MAX_LEN;
}

void frame_list_count_up(frame_list_t *frame_list)
{
    frame_list->count++;
}

void frame_list_count_down(frame_list_t *frame_list)
{
    if(frame_list->count){
        frame_list->count--;
    }
}

void frame_list_pop(frame_list_t *frame_list, frame_t *frame)
{
    if(frame_list_is_empty(frame_list) == true){
        return ;
    }
    
    if(frame != NULL){
        memcpy(frame_list->frame + frame_list->read_index, frame, sizeof(frame_t));
    }
    frame_list_read_index_up(frame_list);
    frame_list_count_down(frame_list);
}

bool frmae_list_append(frame_list_t *frame_list, uint8_t *frame_std_buf, uint16_t buf_len)
{
    frame_t *ptr;
    
    /* 缓存满了，直接丢弃这个包 */
    if(frame_list_is_full(frame_list) == true){
        return false;
    }
    
    ptr = frame_list->frame + frame_list->write_index;
    memcpy(ptr, frame_std_buf, buf_len);
    
    /* 数据域读取索引 */
    ptr->buff_read_index = 0;
    
    /* 更新帧缓存函数索引 */
    frame_list_write_index_up(frame_list);
    frame_list_count_up(frame_list);
    
    return true;
}

uint16_t cal_check_code(uint8_t *data, uint16_t len)
{
    uint16_t i;
#if FRAME_CHECK == FRAME_CHECK_CRC16
    (void)i;
#else /* FRAME_CHECK == FRAME_CHECK_SUM16 */
    uint16_t sum;
    sum = 0;
    for(i = 0; i < len; i++){
        sum += *(data + i);
    }
#endif
    return sum;
}

uint8_t frame_pull_uint8(frame_t *frame)
{
    uint8_t ret;
    if(frame->buff_read_index >= get_frame_data_len(frame)){
        debug("error, %s %d",__func__, __LINE__);
    }
    ret = get_uint8(frame->buff + FRAME_DATA_OFFSET + frame->buff_read_index);
    frame->buff_read_index += sizeof(uint8_t);
    return ret;
}

uint16_t frame_pull_uint16(frame_t *frame)
{
    uint16_t ret;
    ret = get_uint16(frame->buff + FRAME_DATA_OFFSET + frame->buff_read_index);
    frame->buff_read_index += sizeof(uint16_t);
    return ret;
}

uint32_t frame_pull_uint32(frame_t *frame)
{
    uint32_t ret;
    ret = get_uint32(frame->buff + FRAME_DATA_OFFSET + frame->buff_read_index);
    frame->buff_read_index += sizeof(uint32_t);
    return ret;
}

uint64_t frame_pull_uint64(frame_t *frame)
{
    uint64_t ret;
    ret = get_uint64(frame->buff + FRAME_DATA_OFFSET + frame->buff_read_index);
    frame->buff_read_index += sizeof(uint64_t);
    return ret;
}

float frame_pull_flaot(frame_t *frame)
{
    float ret;
    ret = get_float(frame->buff + FRAME_DATA_OFFSET + frame->buff_read_index);
    frame->buff_read_index += sizeof(float);
    return ret;
}

uint16_t frame_pull_string(frame_t *frame, uint8_t *buff, uint16_t buff_len)
{
    uint16_t i;
    for(i = 0; i < buff_len; i++){
        *(buff + i) = frame_pull_uint8(frame);
        if(*(buff +i) == 0){
            break;
        }
    }
    return i;
}

uint16_t frame_pull_bytes(frame_t *frame, uint8_t *buf, uint16_t buff_len)
{
    uint16_t i;
    for(i = 0; i < buff_len; i++){
        *(buf + i) = frame_pull_uint8(frame);
        if(frame->buff_read_index >= get_frame_data_len(frame)){
            return ++i;
        }
    }
    return i;
}

void frame_push_uint8(frame_t *frame, uint8_t var)
{
    set_uint8(frame->buff + FRAME_DATA_OFFSET + frame->buff_write_index, var);
    frame->buff_write_index += sizeof(var);
}

void frame_push_uint16(frame_t *frame, uint16_t var)
{
    set_uint16(frame->buff + FRAME_DATA_OFFSET + frame->buff_write_index, var);
    frame->buff_write_index += sizeof(var);
}

void frame_push_uint32(frame_t *frame, uint32_t var)
{
    set_uint32(frame->buff + FRAME_DATA_OFFSET + frame->buff_write_index, var);
    frame->buff_write_index += sizeof(var);
}

void frame_push_uint64(frame_t *frame, uint64_t var)
{
    set_uint64(frame->buff + FRAME_DATA_OFFSET + frame->buff_write_index, var);
    frame->buff_write_index += sizeof(var);
}

void frame_push_float(frame_t *frame, float var)
{
    set_float(frame->buff + FRAME_DATA_OFFSET + frame->buff_write_index, var);
    frame->buff_write_index += sizeof(var);
}

void frame_init(frame_t *frame, uint8_t session_flag, uint8_t packet_type, uint16_t fun_code)
{
    /* 设置帧头 */
    set_frame_head(frame, FRAME_HEAD);
    /* 设置报文类型 */
    set_frame_packet_type(frame, packet_type);
#if FRAME_SRC_DST
    /* 设置源地址和目的地址 */
    set_frame_src(frame, 0);
    set_frame_dst(frame, 0);
#endif
    /* 设置会话标志 */
    set_frame_session_flag(frame, session_flag);
    /* 设置功能码 */
    set_frame_fun_code(frame, fun_code);

    frame->buff_write_index = 0;
}

void frame_repy_init(frame_t *frame)
{
    frame->buff_write_index = 0;

    /* 设置帧头 */
    set_frame_head(frame, FRAME_HEAD);
    /* 设置报文类型 */
    set_frame_packet_type(frame, get_frame_packet_type(frame));
#if FRAME_SRC_DST
    /* 设置源地址和目的地址 */
    set_frame_src(frame, get_frame_dst(frame));
    set_frame_dst(frame, get_frame_src(frame));
#endif
    /* 设置会话标志 */
    set_frame_session_flag(frame, get_frame_session_flag(frame));
    /* 设置功能码 */
    set_frame_fun_code(frame, get_frame_fun_code(frame));

    frame->buff_write_index = 0;
}

void set_frame_packet_type(frame_t *frame, uint8_t type)
{
    set_uint8(frame->buff + FRAME_PACKET_TYPE_OFFSET, type);
}
void set_frame_session_flag(frame_t *frame, uint8_t session)
{
    set_uint8(frame->buff + FRAME_SESSION_FLAG_OFFSET, session);
}

void set_frame_fun_code(frame_t *frame, uint16_t fun_code)
{
    set_uint16(frame->buff + FRAME_FUN_CODE_OFFSET, fun_code);
}
void set_frame_data_len(frame_t *frame, uint16_t date_len)
{
    set_uint16(frame->buff + FRAME_DATA_LEN_OFFSET, date_len);
}
void set_frame_head(frame_t *frame, uint16_t head)
{
    set_uint16(frame->buff + FRAME_HEAD_OFFSET, head);
}
void set_frame_tail(frame_t *frame, uint16_t tail)
{
    frame_push_uint16(frame,tail);
}

uint8_t *frame_repy_create(frame_t *frame)
{
    uint16_t check_code;
    
    /* 设置数据长度 */
    set_frame_data_len(frame,frame->buff_write_index);
    /* 计算校验值 */
    check_code = cal_check_code(frame->buff + FRAME_CHECK_FIELD_BEGIN,
                                frame->buff_write_index + FRAME_MIN_CHEK_LEN);
    /* 填入校验码*/
    frame_push_uint16(frame, check_code);
    /* 填入帧尾 */
    frame_push_uint16(frame, FRAME_TAIL);
    
    return frame->buff;
}

uint16_t get_frame_repy_len(frame_t *frame)
{
    return frame->buff_write_index;
}

#if FRAME_SRC_DST
uint32_t get_frame_src(frame_t *frame)
{
    return get_uint32(frame->buff + FRAME_SRC_OFFSET);
}

uint32_t get_frame_dst(frame_t *frame)
{
    return get_uint32(frame->buff + FRAME_DST_OFFSET);
}

void set_frame_src(frame_t *frame, uint32_t src)
{
    set_uint32(frame->buff + FRAME_SRC_OFFSET, src);
}
void set_frame_dst(frame_t *frame, uint32_t dst)
{
    set_uint32(frame->buff + FRAME_DST_OFFSET, dst);
}

#endif //if FRAME_SRC_DST

uint8_t get_frame_packet_type(frame_t *frame)
{
    return get_uint8(frame->buff + FRAME_PACKET_TYPE_OFFSET);
}

uint8_t get_frame_session_flag(frame_t *frame)
{
    return get_uint8(frame->buff + FRAME_SESSION_FLAG_OFFSET);
}

uint16_t get_frame_fun_code(frame_t *frame)
{
    return get_uint16(frame->buff + FRAME_FUN_CODE_OFFSET);
}


uint16_t get_frame_data_len(frame_t *frame)
{
    return get_uint16(frame->buff + FRAME_DATA_LEN_OFFSET);
}

uint16_t get_frame_len(frame_t *frame)
{
    return  get_frame_data_len(frame) + FRAME_MIN_LEN;
}

static inline uint8_t  get_uint8(uint8_t *ptr)
{
    return *ptr;
}

static inline uint16_t get_uint16(uint8_t *ptr)
{
#if FFAME_ENDIAN == FRAME_LITTLE_ENDIAN
    return ((uint16_t)get_uint8(ptr) << 8) | (get_uint8(ptr + 1));
#else
    return ((uint16_t)get_uint8(ptr + 1) << 8) | (get_uint8(ptr));
#endif
}

static inline uint32_t get_uint32(uint8_t *ptr)
{
#if FFAME_ENDIAN == FRAME_LITTLE_ENDIAN
    return ((uint32_t)get_uint8(ptr + 0)) << 24| (uint32_t)(get_uint8(ptr + 1)) << 16
                | (uint32_t)(get_uint8(ptr + 2) << 8) | (uint32_t)(get_uint8(ptr + 3));
#else
    return ((uint32_t)get_uint8(ptr) << 3) | (uint32_t)(get_uint8(ptr + 2)) << 8
                | (uint32_t)(get_uint8(ptr + 1)) << 16| (uint32_t)(get_uint8(ptr + 0)) << 24;
#endif
}

static inline uint64_t get_uint64(uint8_t *ptr)
{
#if FFAME_ENDIAN == FRAME_LITTLE_ENDIAN
    return ((uint64_t)get_uint8(ptr + 0)) << 56 | (uint64_t)(get_uint8(ptr + 1)) << 48
                | (uint64_t)(get_uint8(ptr + 2)) << 40 | (uint64_t)(get_uint8(ptr + 3)) << 32
                | (uint64_t)(get_uint8(ptr + 4)) << 24 | (uint64_t)(get_uint8(ptr + 5)) << 16
                | (uint64_t)(get_uint8(ptr + 6)) << 8  | (uint64_t)(get_uint8(ptr + 7)) << 0;
#else
    return ((uint64_t)get_uint8(ptr + 7)) << 0 | (uint64_t)(get_uint8(ptr + 6)) << 8
                | (uint64_t)(get_uint8(ptr + 5)) << 16| (uint64_t)(get_uint8(ptr + 4)) << 24
                | (uint64_t)(get_uint8(ptr + 3)) << 32| (uint64_t)(get_uint8(ptr + 2)) << 40
                | (uint64_t)(get_uint8(ptr + 1)) << 48| (uint64_t)(get_uint8(ptr + 0)) << 56;
#endif
}

static float get_float(uint8_t *ptr)
{
    uint32_t tmp;
    tmp = get_uint32(ptr);
    return *(float *)(&tmp);
}

static void inline set_uint8(uint8_t *ptr, uint8_t var)
{
    *ptr = var;
}

static void set_uint16(uint8_t *ptr, uint16_t var)
{
    uint32_t i, offset;

#if FFAME_ENDIAN == FRAME_LITTLE_ENDIAN
    for(i = 0, offset = 8*(sizeof(var) - 1); i < sizeof(var);
        i++, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#else
    for(i = (sizeof(var) - 1), offset = 8*(sizeof(var) - 1); i >= 0;
        i--, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#endif
}

static void set_uint32(uint8_t *ptr, uint32_t var)
{
    int i, offset;

#if FFAME_ENDIAN == FRAME_LITTLE_ENDIAN
    for(i = 0, offset = 8*(sizeof(var) - 1); i < sizeof(var);
        i++, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#else
    for(i = (sizeof(var) - 1), offset = 8*(sizeof(var) - 1); i >= 0;
        i--, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#endif
}

static void set_uint64(uint8_t *ptr, uint64_t var)
{
    int i, offset;

#if FFAME_ENDIAN == FRAME_LITTLE_ENDIAN
    for(i = 0, offset = 8*(sizeof(var) - 1); i < sizeof(var);
        i++, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#else
    for(i = (sizeof(var) - 1), offset = 8*(sizeof(var) - 1); i >= 0;
        i--, offset -= 8){
        set_uint8(ptr + i, (var >> offset) & 0xff);
    }
#endif
}

static void set_float(uint8_t *ptr, float var)
{
    set_uint32(ptr, *(uint32_t *)&var);
}

void print_frame(frame_t *frame)
{
    uint8_t *ptr;
    ptr = frame->buff;
    printf("hex:{ ");
    for(uint16_t i = 0; i < get_frame_len(frame); i++){
        printf("%02x ",*(ptr + i));
    }

    printf("}\n");
    printf("head:     0x%04x\n", FRAME_HEAD);
    printf("pkg type: 0x%02x\n",get_frame_packet_type(frame));
    printf("src:      0x%08x\n", get_frame_src(frame));
    printf("dst:      0x%08x\n", get_frame_dst(frame));
    printf("sflag:    0x%02x\n", get_frame_session_flag(frame));
    printf("fcode:    0x%04x\n", get_frame_fun_code(frame));
    printf("len:      0x%02x\n", get_frame_data_len(frame));

}


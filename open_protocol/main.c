//
//  main.c
//  open_protocol
//
//  Created by Eywen on 2021/2/3.
//

#include <stdio.h>
#include "open_protocol.h"
#include <stdint.h>

int main(int argc, const char * argv[]) {
    // insert code here...
    printf("Hello, World!\n");

    frame_t frame;

    frame_init(&frame, 0x01, 0x02, 0x1234);
    set_frame_dst(&frame,0xaabbccdd);
    set_frame_src(&frame,0xbbccddee);
    frame_push_uint32(&frame,0x12345678);
    frame_push_uint16(&frame,0x1234);
    frame_repy_create(&frame);

    print_frame(&frame);

    return 0;
}

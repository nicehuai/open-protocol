//
//  handle.c
//  open_protocol
//
//  Created by Eywen on 2021/2/6.
//

#include "handle.h"

static bool handle_shake(frame_t *frame);
static bool handle_upgrade(frame_t *frame);
static bool handle_file_trans(frame_t *frame);
static bool handle_heartbeat(frame_t *frame);
static void send_reply_data(frame_t *frame);

const handle_t frame_handle_list[] =
{
    {0x01, handle_shake},       /* 握手协议 */
    {0x02, handle_heartbeat},
    {0x04, handle_upgrade},     /* 升级协议 */
    {0x05, handle_file_trans},  /* 传输文件协议 */
};

bool handle_shake(frame_t *frame)
{
    debug("__fun__");
    return true;
}

bool handle_upgrade(frame_t *frame)
{
    uint8_t sub_fun;

    /* 升级子命令 */
    sub_fun = frame_pull_uint8(frame);

    switch (sub_fun){
    case 0x01: /*请求升级命令*/
        do{
            uint32_t file_size = frame_pull_uint32(frame);
            uint16_t packet_count = frame_pull_uint16(frame);
            uint8_t  soft_version[SOFT_VERTION_LEN];

            frame_pull_string(frame,soft_version,SOFT_VERTION_LEN);

            /* 判断是否满足升级条件 */

            /* 回复结果 */

        }while(0);
        break;
    case 0x02: /* 升级结果 */
        do{
            /* 回复结果 */

        }while(0);
        break;
    case 0x03: /* 中途终止升级 */
        break;
    default:
        break;
    }

    debug("__fun__");
    return true;
}

/* 文件传输协议 */
static bool handle_file_trans(frame_t *frame)
{
    uint8_t sub_fun;

    /* 升级子命令 */
    sub_fun = frame_pull_uint8(frame);

    switch (sub_fun) {
    case (0x01): /* 请求传输文件 */
        do{
            uint32_t file_size = frame_pull_uint32(frame);
            uint16_t packet_count = frame_pull_uint16(frame);
            uint8_t location_type = frame_pull_uint8(frame);
            uint32_t store_addr = frame_pull_uint32(frame);

        }while(0);
        break;
    case (0x02): /* 分包文件内容 */
        do{
            uint16_t packet_index = frame_pull_uint16(frame);
            u_int8_t bytes[FILE_TRANS_PER_LEN];
            frame_pull_bytes(frame, bytes, FILE_TRANS_PER_LEN);

        }while(0);
        break;
    case (0x03): /* 文件传输完毕 */
        do{
            uint32_t crc = frame_pull_uint32(frame);
        }while(0);
        break;
    case (0x04): /* 中途终止传输 */
        do{

        }while(0);
        break;
    default:
        break;
    }

    send_reply_data(frame);
}

/* 发送回复数据 */
static void send_reply_data(frame_t *frame)
{

    for(uint16_t i = 0 ; i < get_frame_len(frame); i++){
        printf("%x ",*(frame->buff + i));
    }
}

static bool handle_heartbeat(frame_t *frame)
{
    frame_repy_init(frame);
    frame_repy_create(frame);
    send_reply_data(frame);
}


//
//  open_protocol.h
//  open_protocol
//
//  Created by Eywen on 2021/2/3.
//

#ifndef open_protocol_h
#define open_protocol_h

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#define FRAME_DEBUG 1

#if FRAME_DEBUG
#define debug(format, ...)  printf(format, ##__VA_ARGS__)
#else
#define debug(...)
#endif

#define FRAME_BUF_LIST_MAX_LEN     2             /* 最多能缓存的完整帧数量 */

#define FRAME_BIG_ENDIAN           1        /* 大端模式。高位在低字节，低位在高子节*/
#define FRAME_LITTLE_ENDIAN        (!FRAME_BIG_ENDIAN)   /* 小端标志 */
#define FFAME_ENDIAN               FRAME_LITTLE_ENDIAN  /* 帧数据大小端定义 */

#define FRAME_CHECK_CRC16          0                     /* CRC16方式校验帧 */
#define FRAME_CHECK_SUM16          (!FRAME_CHECK_CRC16)  /* 模0xffff方式校验帧 */
#define FRAME_CHECK                FRAME_CHECK_SUM16

#define FRAME_SRC_DST              1                     /* 帧地址开关，用于多设备通信 */

#if FRAME_SRC_DST
#define FRAME_BROADCAST_ADDR       0xffffffff        /* 广播地址 */
#endif


#define FRAME_HEAD                  0xfefe
#define FRAME_TAIL                  0xefef

#define FRAME_HEAD_OFFSET           0
#define FRAME_PACKET_TYPE_OFFSET    2

#define FRAME_CHECK_FIELD_BEGIN     2           /* 校验开始的内容 */

#if FRAME_SRC_DST
#define FRAME_ADDR_LEN              4
#define FRAME_SRC_OFFSET            (FRAME_PACKET_TYPE_OFFSET + 1)
#define FRAME_DST_OFFSET            (FRAME_SRC_OFFSET + FRAME_ADDR_LEN)
#define FRAME_SESSION_FLAG_OFFSET   (FRAME_DST_OFFSET + FRAME_ADDR_LEN)
#define FRAME_CHECK_FIELD_MIN_LEN   (6 + FRAME_ADDR_LEN*2)  /* 最小校验长度，数据域为空情况下 */
#define FRAME_MIN_LEN               (12 + FRAME_ADDR_LEN*2)          /* 帧最小长度 */
#define FRAME_MIN_CHEK_LEN          (6 + FRAME_ADDR_LEN*2)   /* 校验最小长度 */
#else
#define FRAME_SESSION_FLAG_OFFSET   3
#define FRAME_CHECK_FIELD_MIN_LEN   6           /* 最小校验长度，数据域为空情况下 */
#define FRAME_MIN_LEN               12          /* 帧最小长度 */
#define FRAME_MIN_CHEK_LEN          6           /* 校验最小长度 */
#endif

#define FRAME_MAX_DATA_LEN          1024              /* 帧数据最大长度*/
#define FRAME_MAX_LEN               (FRAME_MAX_DATA_LEN + FRAME_MIN_LEN)/* 帧总长最大长度*/

#define FRAME_FUN_CODE_OFFSET       (FRAME_SESSION_FLAG_OFFSET + 1)
/* 数据长度字段相对帧头的偏移 */
#define FRAME_DATA_LEN_OFFSET       (FRAME_FUN_CODE_OFFSET + 2)
/* 数据区域相对帧头的偏移 */
#define FRAME_DATA_OFFSET           (FRAME_DATA_LEN_OFFSET + 2)
/* 校验码相对帧头的最小偏移 */
#define FRAME_CHECK_CODE_MIN_OFFSET (FRAME_DATA_OFFSET)
/* 帧尾相对帧头的最小偏移 */
#define FRAME_TAIL_MIN_OFFSET       (FRAME_CHECK_CODE_MIN_OFFSET + 2)


/* 报文框架,小端模式，低字节在低位 */
typedef struct _frame{
    //uint16_t head;                       /* 帧头标志 */
#if (FRAME_SRC_DST == 1)
    //uint32_t src_addr;                  /* 源地址 */
    //uint32_t dst_addr;                  /* 目的地址 */
#endif
    //uint8_t  session_flag;               /* 会话标志 */
    //uint8_t  packet_type;                /* 报文类型 */
    //uint16_t fun_code;                   /* 功能码 */
    //uint16_t data_le   n;                /* 数据长度 */
//    union {
//        uint8_t req_buff[FRAME_MAX_LEN];       /* 接收数据内容缓存 */
//        uint8_t repy_buff[FRAME_MAX_LEN];      /* 回复数据内容缓存*/
//    }buff;
    uint8_t buff[FRAME_MAX_LEN];

    //uint16_t check_code;                 /* 校验码，会话标志至数据的内容校验结果 */
    //uint16_t tail;                       /* 帧尾 */
    uint16_t buff_write_index;
    uint16_t buff_read_index;            /* 数据读取索引 */
} frame_t;

typedef struct _frame_ist{
    frame_t frame[FRAME_BUF_LIST_MAX_LEN];
    uint8_t write_index;
    uint8_t read_index;
    uint8_t count;
} frame_list_t;

typedef enum _frame_status{
    ok_integrity,           /* 具备完整的帧头和帧尾 */
    error_integrity,        /* 没有完整的帧头或帧尾 */
    error_len,              /* 长度错误 */
    error_check,            /* 帧内容校验错误 */
    error_head,             /* 错误的头部 */
    error_tail,             /* 错误的尾部 */
    error_packet_type,      /* 不支持的报文类型 */
    error_fun_code,         /* 不支持的功能码 */
}frame_status_t;


void frame_list_init(frame_list_t *frame_list);
frame_status_t deframe(uint8_t *frame_data, uint16_t frame_len);
uint8_t *frame_repy_create(frame_t *frame);
void frame_init(frame_t *frame, uint8_t session_flag, uint8_t packet_type, uint16_t fun_code);
void frame_repy_init(frame_t *frame);
uint16_t get_frame_repy_len(frame_t *frame);
uint8_t frame_pull_uint8(frame_t *frame);
uint16_t frame_pull_uint16(frame_t *frame);
uint32_t frame_pull_uint32(frame_t *frame);
uint64_t frame_pull_uint64(frame_t *frame);
float frame_pull_flaot(frame_t *frame);
uint16_t frame_pull_string(frame_t *frame, uint8_t *buff, uint16_t buff_len);
uint16_t frame_pull_bytes(frame_t *frame, uint8_t *buf, uint16_t buff_len);
uint8_t get_frame_packet_type(frame_t *frame);
uint8_t get_frame_session_flag(frame_t *frame);
uint16_t get_frame_fun_code(frame_t *frame);
uint16_t get_frame_data_len(frame_t *frame);
uint16_t get_frame_len(frame_t *frame);
#if FRAME_SRC_DST
uint32_t get_frame_src(frame_t *frame);
uint32_t get_frame_dst(frame_t *frame);
void set_frame_src(frame_t *frame, uint32_t src);
void set_frame_dst(frame_t *frame, uint32_t dst);
#endif //if FRAME_SRC_DST


void frame_push_uint8(frame_t *frame, uint8_t var);
void frame_push_uint16(frame_t *frame, uint16_t var);
void frame_push_uint32(frame_t *frame, uint32_t var);
void frame_push_uint64(frame_t *frame, uint64_t var);
void frame_push_float(frame_t *frame, float var);


void set_frame_packet_type(frame_t *frame, uint8_t type);
void set_frame_session_flag(frame_t *frame, uint8_t session);
void set_frame_fun_code(frame_t *frame, uint16_t fun_code);
void set_frame_data_len(frame_t *frame, uint16_t date_len);
void set_frame_head(frame_t *frame, uint16_t head);
void set_frame_tail(frame_t *frame, uint16_t tail);
void print_frame(frame_t *frame);


#endif /* open_protocol_h */
